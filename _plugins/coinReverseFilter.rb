
module Jekyll
	module ReverseArray
		
		def coin_reverse(input)
			@input = input.split(",").reverse!
			@input[0] = @input[0].rstrip.lstrip
			@input
		end
	end
end

Liquid::Template.register_filter(Jekyll::ReverseArray)
