
module Jekyll
	module TitleStyleRemove
		
		def rm_style(input)
			@input = input.gsub(/\*/, '')
			@input
		end
	end
end

Liquid::Template.register_filter(Jekyll::TitleStyleRemove)
