
module Jekyll
	module ReverseArray
		
		def reverse(input)
			@suffixes = [
				"CPA",
				"D.C.",
				"D.D.",
				"D.D.S.",
				"D.M.D.",
				"D.O.",
				"D.V.M.",
				"Ed.D.",
				"Esq.",
				"II",
				"III",
				"IV",
				"J.D.",
				"Jr.",
				"LL.D.",
				"M.D.",
				"O.D.",
				"Ph.D.",
				"R.N.",
				"R.N.C.",
				"Sr."
				]
			@input = input.split(",").reverse!
			@input[0] = @input[0].rstrip.lstrip
			@input[0] = @input[0] << " "
			@suffix_check = @input.join(", ")
			@suffix = @suffixes.find { |suffix| @suffix_check.include?(suffix)}

			if @suffix.nil?
				@input
			else
				@input = @suffix_check.split(@suffix << ", ")
				@input[1] = @input[1] << ", " << @suffix.chomp(", ")
				@input
			end
		end
	end
end

Liquid::Template.register_filter(Jekyll::ReverseArray)
