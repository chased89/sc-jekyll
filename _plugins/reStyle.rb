
module Jekyll
	module TitleStyleReplace
		
		def re_style(input)
			@input = input.gsub(/\*(.+?)\*/, '<span class="i">\1</span>')
			@input
		end
	end
end

Liquid::Template.register_filter(Jekyll::TitleStyleReplace)
