require 'yaml'
require 'erb'

include ERB::Util
# @TODO: Need to add in formatting for different journals
module Jekyll
	class RenderIndexTag < Liquid::Tag
		
		def initialize(tag_name, yamlFile, tokens)
			super
			@yamlFile = yamlFile

			super
			@output = ""

			super
			@merged_attr = {}
		end

		def render(context)
			generateIndex(context, context[@yamlFile.strip])
			"#{@output}"
		end

		def generateOutput(article_section, articles)
			articles.each do |article|
		       	article.each do |art_title, metadata|
		            @title_for_coins = url_encode(art_title)
		            if article_section.eql?("articles") and articles.index(article) == 0
		                @output << "\r<h3 class=\"center\">\rTABLE OF CONTENTS\r</h3>"
		            end
		            if art_title.include?("*")
		            	@title_for_coins.gsub!(/\*(.*)\*/, "\\1")
		                art_title = art_title.dup.gsub!(/\*(.*)\*/, "<span class=\"i\">\\1 </span>")
		            end
		            if art_title.match(/\s\s\s/)
		                art_title = art_title.dup.gsub!(/(\s\s\s)(.*$)/, "<br>\r <span class=\"normal\"> \\2</span>")
		            end
		            if art_title.include?(":")
		                if art_title.match(/(<span class="i">)(.*?)(:)/)
		                    @output << "\r<p>\r<span class=\"b\">" \
		                        << art_title << "</span> <br>\r"
		                else
		                    @splitKey = art_title.split(":")
		                    for bit in @splitKey
		                        if @splitKey.index(bit) == 0
		                            @output << "\r<p>\r" << \
		                            "<span class=\"b\">" << bit << ":</span>"
		                        else
		                            @output << "<span class=\"normal\">" \
		                            << bit << "</span> <br>\r"
		                        end
		                    end
		                end
		            else
		                @output << "\r<p>\r<span class=\"b\">" \
		                        << art_title << "</span> <br>\r"
		            end
		            metadata.each do |art_attr|
		                @merged_attr.merge!(art_attr)
		            end
		            if @merged_attr.key?("authors")
		                @splitAuthors = @merged_attr.fetch("authors").split(",")
		                if @splitAuthors.length > 2
		                    @splitAuthors.each do |author|
		                        if @splitAuthors.index(author) == 0
		                             @output << "<span class=\"i\">" << author << \
		                             ","
		                        elsif @splitAuthors.index(author) \
		                        == @splitAuthors.length - 1
		                            @output << "<span class=\"normal\">and" << \
		                            "</span>" << author << "</span>"
		                        else
		                            @output << author << ","
		                        end
		                    end
		                else
		                  @splitAuthors.each do |author|
		                      if @splitAuthors.index(author) == 0
		                             @output << "<span class=\"i\">" << author << \
		                             ","
		                        else
		                            @output << author << "</span>"
		                        end
		                    end
		                end
		            end
		            if @merged_attr.key?("filename")
		                if @merged_attr.key?("pdfSize")
		                    @output << "<span class=\"artlink\"> <a href=\"pdf/" << \
		                    @merged_attr.fetch("filename") << ".pdf\"> PDF</a> [" << \
		                    @merged_attr.fetch("pdfSize") << "]</span>"
		                end
		                if @merged_attr.key?("htmlSize")
		                    @output << "<span class=\"artlink\"> <a href=\"" << \
		                    @merged_attr.fetch("filename") << ".html\"> HTML</a> [" << \
		                    @merged_attr.fetch("htmlSize") << "]</span>"
		                end            
		            end
		            if article_section.eql?("articles")
		                @output << "\r<span class='Z3988' title='url_ver=Z39.88-2004" << \
		                "&amp;ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fzotero.org%3A2" << \
		                "&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Ajournal&amp;rft.genre=article&amp;" << \
		                "rft.atitle=" << @title_for_coins << "&amp;rft.jtitle=" \
		                << @journal_coin << @volume << "&amp;" \
		                << "rft.issue=" << @issue << "&amp;rft.ssn=" << @season << \
		                "&amp;"

		                if @merged_attr.key?("authors")
		                    @splitAuthors.each do |author|
		                        author_name = author.split(" ")
		                        firstname = author_name.first(author_name.length - 1).join(" ")
		                        lastname = author_name.last
		                        if @splitAuthors.length > 2
		                            if @splitAuthors.index(author) == 0
		                                @output << "rft.aufirst=" << url_encode(firstname) << "&amp;" << \
		                                "rft.aulast=" << url_encode(lastname) << "&amp;"
		                            else
		                                firstname << " "
		                                
		                                @output << "rft.au=" << url_encode(firstname) << url_encode(lastname) << "&amp;"
		                            end
		                        else
		                            @output << "rft.aufirst=" << url_encode(firstname) << "&amp;" << \
		                                "rft.aulast=" << url_encode(lastname) << "&amp;" 
		                            
		                            firstname << " "
		                            
		                            @output << "rft.au=" << url_encode(firstname) << url_encode(lastname) << "&amp;"
		                        end
		                    end
		                end
		                @output << "rft.date=" << @year << @journal_issn
		            end
		            @output << "\r</p>"
		        end
		    end
		end

		def generateIndex(context, filename)
			jekyllSite = context.registers[:site]
			dir = jekyllSite.source+'/'
			filename = dir << filename

			@articles = YAML.load_file(filename)
			@coins_journal_hash = {
				"ALAN" => "The%20ALAN%20Review&amp;rft.stitle=ALANrft.volume=",
				"JCTE" => "Journal%20of%20Career%20and%20Technical%20Education&amp;rft.stitle=JCTE&amp;rft.volume=",
				"JOTS" => "Journal%20of%20Technology%20Studies&amp;rft.stitle=JOTS&amp;rft.volume=",
				"JTE" => "Journal%20of%20Technology%20Education&amp;rft.stitle=JTErft.volume=",
				"VALib" => "Virginia%20Libraries&amp;rft.stitle=VALibrft.volume="}
			@coins_issn_hash = {
				"ALAN" => "&amp;rft.issn=1547-741X'></span>",
				"JCTE" => "&amp;rft.issn=1533-1830'></span>",
				"JOTS" => "&amp;rft.issn=1045-1064'></span>",
				"JTE" => "&amp;rft.issn=1541-9258'></span>",
				"VALib" => "&amp;rft.issn=1086-9751'></span>"}
			@articles.each_value do |article_section|
			    if @articles.key(article_section).eql?("journal_info")
		            @journal = article_section.fetch("journal")
		            @coins_journal_hash.each_key do |key|
					    if key.eql?(@journal)
					        @journal_coin = @coins_journal_hash.fetch(key)
					        @journal_issn = @coins_issn_hash.fetch(key)
					    end
					end 
		            @volume = article_section.fetch("volume").to_s
		            @issue = article_section.fetch("issue").to_s
		            @season = article_section.fetch("season")
		            @year = article_section.fetch("year").to_s
		        else
		            generateOutput(@articles.key(article_section), article_section)
		        end
		    end
		end
	end
end

Liquid::Template.register_tag('create_index', Jekyll::RenderIndexTag)